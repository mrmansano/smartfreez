/*
 * LCDModule.h
 *
 *  Created on: 17/05/2014
 *      Author: marcelo
 */

#ifndef LCDMODULE_H_
#define LCDMODULE_H_

#include "GpioDevice.h"

typedef struct {
	GpioPinNum data7Pin;
	GpioPinNum data6Pin;
	GpioPinNum data5Pin;
	GpioPinNum data4Pin;
} LCDDataPins;

class LCDModule: public GpioDevice {
public:
	LCDModule(GpioPinNum rsPin, GpioPinNum enPin, LCDDataPins dataPins);
	virtual ~LCDModule();

	void initLCD();
	void clear();
	void pulse();
	void sendData(const char data);
	void sendInstruction(const char inst);
	void writeText(const char* txt, int row, int col);

private:
	LCDDataPins mDataPins;
	GpioPinNum mRsPin;
	GpioPinNum mEnPin;
};

#endif /* LCDMODULE_H_ */
