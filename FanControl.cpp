
/*****************************************************************************/
/**
 * \file	FanControl.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include "FanControl.h"

/******************************************************************/
/*                       Local Database                           */
/******************************************************************/
/*obs: put here global vars (if you need them)*/

/******************************************************************/
/*                       Methods                                  */
/******************************************************************/

FanControl::FanControl(GpioPinNum fanCrtlpin) : GpioDevice() {
	mFanCrtlPin = fanCrtlpin;
	addPin(mFanCrtlPin, "FAN_CRTL", GPIO_PIN_OUTPUT);
}

FanControl::~FanControl() {
}

void FanControl::turnOn() {
	writePin(mFanCrtlPin, GPIO_PIN_HIGH);
	mStatus = FAN_ON;
}

void FanControl::turnOff() {
	writePin(mFanCrtlPin, GPIO_PIN_LOW);
	mStatus = FAN_OFF;
}

void FanControl::setStatus(FanStatus status) {
	mStatus = status;
}

FanStatus FanControl::getStatus() const {
	return mStatus;
}

GpioPinNum FanControl::getFanCrtlPin() const {
	return mFanCrtlPin;
}

void FanControl::setFanCrtlPin(GpioPinNum fanCrtlPin) {
	remPin(mFanCrtlPin);
	mFanCrtlPin = fanCrtlPin;
	addPin(mFanCrtlPin, "FAN_CRTL", GPIO_PIN_OUTPUT);
}
