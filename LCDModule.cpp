/*
 * LCDModule.cpp
 *
 *  Created on: 17/05/2014
 *      Author: marcelo
 */

#include <unistd.h>
#include "LCDModule.h"

LCDModule::LCDModule(GpioPinNum rsPin, GpioPinNum enPin, LCDDataPins dataPins) :
GpioDevice()
{
	mRsPin = rsPin;
	mEnPin = enPin;
	mDataPins = dataPins;

	addPin(mRsPin, "RS", GPIO_PIN_OUTPUT);
	addPin(mEnPin, "EN", GPIO_PIN_OUTPUT);
	addPin(mDataPins.data7Pin, "D7", GPIO_PIN_OUTPUT);
	addPin(mDataPins.data6Pin, "D6", GPIO_PIN_OUTPUT);
	addPin(mDataPins.data5Pin, "D5", GPIO_PIN_OUTPUT);
	addPin(mDataPins.data4Pin, "D4", GPIO_PIN_OUTPUT);
}

LCDModule::~LCDModule() {
	// TODO Auto-generated destructor stub
}

void LCDModule::initLCD() {
	// EN = 1
	writePin(mEnPin, GPIO_PIN_HIGH);
	// RS = 0
	writePin(mRsPin, GPIO_PIN_LOW);

	for (int i = 0 ; i < 3; ++i) {
		// Escreve 0x30 (0b0011) no Data
		writePin(mDataPins.data7Pin, GPIO_PIN_LOW);
		writePin(mDataPins.data6Pin, GPIO_PIN_LOW);
		writePin(mDataPins.data5Pin, GPIO_PIN_HIGH);
		writePin(mDataPins.data4Pin, GPIO_PIN_HIGH);
		// Dá um pulso no EN
		pulse();
		// Delay de ~5ms
		usleep(5000);
	}

	// Escreve 0x20 (0b0010) no Data
	writePin(mDataPins.data7Pin, GPIO_PIN_LOW);
	writePin(mDataPins.data6Pin, GPIO_PIN_LOW);
	writePin(mDataPins.data5Pin, GPIO_PIN_HIGH);
	writePin(mDataPins.data4Pin, GPIO_PIN_LOW);

	// Mais um pulso no EN
	pulse();

	// Envia as instruções
	sendInstruction(0x01);
	sendInstruction(0x06);
	sendInstruction(0x0c);
}

void LCDModule::clear() {
	sendInstruction(0x01);
}

void LCDModule::pulse() {
	// EN = 1
	writePin(mEnPin, GPIO_PIN_HIGH);
	// delay de ~20us
	usleep(20);
	// EN = 0
	writePin(mEnPin, GPIO_PIN_LOW);
}

void LCDModule::sendInstruction(const char inst) {
	// RS = 0
	writePin(mRsPin, GPIO_PIN_LOW);
	// Escreve 4 bits mais significativos de inst no Data (PB7-PB4)
	writePin(mDataPins.data7Pin, (GpioPinValue)((inst & 0x80) >> 7));
	writePin(mDataPins.data6Pin, (GpioPinValue)((inst & 0x40) >> 6));
	writePin(mDataPins.data5Pin, (GpioPinValue)((inst & 0x20) >> 5));
	writePin(mDataPins.data4Pin, (GpioPinValue)((inst & 0x10) >> 4));

	// Dá um pulso no EN
	pulse();

	// Delay de ~100us
	usleep(100);

	// Escreve 4 bits menos significativos de inst no Data (PB7-PB4)
	writePin(mDataPins.data7Pin, (GpioPinValue)((inst & 0x08) >> 3));
	writePin(mDataPins.data6Pin, (GpioPinValue)((inst & 0x04) >> 2));
	writePin(mDataPins.data5Pin, (GpioPinValue)((inst & 0x02) >> 1));
	writePin(mDataPins.data4Pin, (GpioPinValue)(inst & 0x01));

	// Dá um pulso no EN
	pulse();

	// Delay de ~5ms
	usleep(5000);
}

void LCDModule::sendData(const char data) {
	// RS = 1
	writePin(mRsPin, GPIO_PIN_HIGH);
	// Escreve 4 bits mais significativos de inst no Data (PB7-PB4)
	writePin(mDataPins.data7Pin, (GpioPinValue)((data & 0x80) >> 7));
	writePin(mDataPins.data6Pin, (GpioPinValue)((data & 0x40) >> 6));
	writePin(mDataPins.data5Pin, (GpioPinValue)((data & 0x20) >> 5));
	writePin(mDataPins.data4Pin, (GpioPinValue)((data & 0x10) >> 4));

	// Dá um pulso no EN
	pulse();

	// Delay de ~100us
	usleep(100);

	// Escreve 4 bits menos significativos de inst no Data (PB7-PB4)
	writePin(mDataPins.data7Pin, (GpioPinValue)((data & 0x08) >> 3));
	writePin(mDataPins.data6Pin, (GpioPinValue)((data & 0x04) >> 2));
	writePin(mDataPins.data5Pin, (GpioPinValue)((data & 0x02) >> 1));
	writePin(mDataPins.data4Pin, (GpioPinValue)(data & 0x01));

	// Dá um pulso no EN
	pulse();

	// Delay de ~5ms
	usleep(5000);
}

void LCDModule::writeText(const char* txt, int row, int col) {
	unsigned char address = 0;

	switch(row)
	{
	case 0:
		address = 0x80 + col;
		break;

	case 1:
		address = 0xC0 + col;
		break;
	}

	// Envia o endereço inicial do LCD como instrução
	sendInstruction(address);

	// Envia todos os caracteres da string de entrada
	while(*txt) sendData(*txt++);
}
