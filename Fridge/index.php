<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">


      <meta http-equiv="refresh" content="5" >
      <link rel="stylesheet" href="Metro-UI-CSS-master/css/metro-bootstrap.css">
      <link rel="stylesheet" href="Metro-UI-CSS-master/docs/css/iconFont.css">

      <script src="Metro-UI-CSS-master/docs/js/jquery/jquery.min.js"></script>
      <script src="Metro-UI-CSS-master/docs/js/jquery/jquery.widget.min.js"></script>
      <script src="Metro-UI-CSS-master/docs/js/load-metro.js"></script>
      <script src="Metro-UI-CSS-master/docs/js/github.info.js"></script>
      <script src="Metro-UI-CSS-master/docs/js/metro/metro.min.js"></script>

      <title>GI</title>

      <style>
      .container {
    width: 1040px;
  }
      </style>

<?php
error_reporting ( E_ALL );
set_time_limit ( 0 );
ob_implicit_flush ();
$address = '127.0.0.1';
$port = 55555;

$socket = socket_create ( AF_INET, SOCK_STREAM, SOL_TCP );
if (! $socket) {
    echo 'Socket creation failed. Reason ' . socket_strerror ( socket_last_error () ) . "\n";
}

$result = socket_connect ( $socket, $address, $port );
if ($result == false) {
    echo "socket_connect() failed.\nReason: ($result) " . socket_strerror ( socket_last_error ( $socket ) ) . "\n";
}

$in = "G_DATA";
$out = '';
$out_length = 18;

socket_write($socket, $in, strlen($in));

$connection = "ON";
$system = "FAIL";
$freezer = "FAIL";
$fan = "FAIL";
$pelt = "FAIL";
$humidity = "FAIL";
$mon = "FAIL";
$out = socket_read($socket, $out_length);
$data = explode(";",$out);

if ($data[0] == "1") {
    $system = "ON";
} else {
    $system = "OFF";
}

$freezer = $data[1];
$mon = $data[2];
$humidity = $data[3];

if ($data[0] == "1") {
    $fan = "ON";
} else {
    $fan = "OFF";
}

$pelt = $data[5];

socket_close($socket);
?>
</head>
<body class="metro">

    <header class="margin20 nrm nlm">
    <a class="place-center" href="#" title>
    </a>
    </header>

    <div class="container">

    <div style="margin-top: 25px">

    <a class="place-center" href="#" title>
    <h1> <img src="MansanoCreationsSimbol.png" class="shadow" data-src="." alt="120x120" style="width: 120px; height: 120px;"> Geladeira Inteligente</h1>
    </a>

    </div>

    <div id="buttons" style="margin-top: 35px">

    <div class="tile bg-orange">
    <div class="tile-content icon">
    <i class="icon-broadcast">
    </i>
    </div>

    <div id="btnConnection" class="tile-status">
    <span class="label">Conexão</span>
    <span name="btnConnection"class="badge"><?php echo $connection ?></span>
    </div>
    </div>

    <div class="tile bg-yellow">
    <div class="tile-content icon">
    <i class="icon-switch">
    </i>
    </div>

    <div id="btnFridgeStatus" class="tile-status">
    <span class="label">Sistema</span>
    <span class="badge"><?php echo $system ?></span>
    </div>
    </div>

    <div class="tile bg-lightGreen">
    <div class="tile-content icon">
    <i class="icon-Celsius">
    </i>
    </div>

    <div id="btnFridgeTemperature" class="tile-status">
    <span class="label">Geladeira</span>
    <span class="badge"><?php echo $freezer ?></span>
    </div>
    </div>


    <div class="tile bg-darkPink">
    <div class="tile-content icon">
    <i class="icon-air">
    </i>
    </div>

    <div id="btnFan" class="tile-status">
    <span class="label">FAN</span>
    <span class="badge"><?php echo $fan ?></span>
    </div>
    </div>

    <div class="tile bg-darkGreen">
    <div class="tile-content icon">
    <i class="icon-pinterest">
    </i>
    </div>

    <div id="btnPeltier" class="tile-status">
    <span class="label">Peltier</span>
    <span class="badge"><?php echo $pelt ?></span>
    </div>
    </div>

    <div class="tile bg-lightRed">
    <div class="tile-content icon">
    <i class="icon-thermometer">
    </i>
    </div>

    <div id="btnSystemTemperature" class="tile-status">
    <span class="label">Critica</span>
    <span class="badge"><?php echo $mon; ?></span>
</div>
</div>

<div class="tile bg-lightBlue">
    <div class="tile-content icon">
    <i class="icon-droplet">
    </i>
    </div>

    <div id="btnHumidity" class="tile-status">
    <span class="label">Umidade (%)</span>
    <span class="badge"><?php echo $humidity ?></span>
    </div>
    </div>

    </div>

    <div id="configs" style="margin-top: 150px">

    <h2><br/>Configurações</h2>

    <form action="client_connect.php" method="post">

    <label>Temperatura de refrigeração (em ºC)</label>
    <input name="iptFridgeTemperature" type="number" placeholder="Digite o valor">

    <label>Temperatura de (des)acionamento do fan (em ºC)</label>
    <input name="iptFanTemperature" type="number" placeholder="Digite o valor">

    <label>Temperatura de desligamento do sistema (em ºC)</label>
    <input name="iptShutDownTemperature" type="number" placeholder="Digite o valor">

    <h2> </h2>

    <input id="btnSubmitConfiguration" type="submit" value="Atualizar configurações do sistema">
    </form>

    </div>

    </div>
    </body>
    </html>
