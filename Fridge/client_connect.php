<!DOCTYPE html>
<html>
    <head>
	    <meta charset="utf-8">
            <link rel="stylesheet" href="Metro-UI-CSS-master/css/metro-bootstrap.css">
            <link rel="stylesheet" href="Metro-UI-CSS-master/docs/css/iconFont.css">
           
	    <script src="Metro-UI-CSS-master/docs/js/jquery/jquery.min.js"></script>
            <script src="Metro-UI-CSS-master/docs/js/jquery/jquery.widget.min.js"></script>
	    <script src="Metro-UI-CSS-master/docs/js/load-metro.js"></script>
	    <script src="Metro-UI-CSS-master/docs/js/github.info.js"></script>
            <script src="Metro-UI-CSS-master/docs/js/metro/metro.min.js"></script>

	    <title>GI</title>

	    <style>
		    .container {
			    width: 1040px;
		    }
	    </style>
    </head>
    <body>
        <?php
            error_reporting ( E_ALL );
            set_time_limit ( 0 );

            ob_implicit_flush ();

            $address = '127.0.0.1';
            $port = 55555;

            $commands = array (
		            "MSG_ON" => "T_ON",
		            "MSG_OFF" => "T_OFF",
		            "MSG_FREEZE" => "T_FREEZE_",
		            "MSG_WARM" => "T_WARM_",
		            "MSG_FAN_ON" => "T_FAN_ON",
		            "MSG_FAN_OFF" => "T_FAN_OFF",
                    "MSG_FREEZE_VAL" => "V_FREEZE#",
                    "MSG_HOT_VAL" => "V_HOT#",
                    "MSG_CRITICAL_VAL" => "V_CRIT#",
            );

            $socket = socket_create ( AF_INET, SOCK_STREAM, SOL_TCP );
            if (! $socket) {
	            echo 'Socket creation failed. Reason ' . socket_strerror ( socket_last_error () ) . "\n";
            } 

            $result = socket_connect ( $socket, $address, $port );
            if ($result == false) {
	            echo "socket_connect() failed.\nReason: ($result) " . socket_strerror ( socket_last_error ( $socket ) ) . "\n";
            } 
            $sts = true;
            $in = $commands["MSG_FREEZE_VAL"] . $_POST["iptFridgeTemperature"];
            $out = '';
            $out_length = 5;
		
            socket_write($socket, $in, strlen($in));

            $out = socket_read($socket, $out_length);
            if ($out == "S_ERR") {
                $sts = false;
            } 

            $in = $commands["MSG_HOT_VAL"] . $_POST["iptFanTemperature"];
            $out = '';
            $out_length = 5;
		
            socket_write($socket, $in, strlen($in));
            $out = socket_read($socket, $out_length);
            if ($out == "S_ERR") {
                $sts = false;
            }

            $in = $commands["MSG_CRITICAL_VAL"] . $_POST["iptShutDownTemperature"];
            $out = '';
            $out_length = 5;
		
            socket_write($socket, $in, strlen($in));
            $out = socket_read($socket, $out_length); 
            if ($out == "S_ERR") {
                $sts = false;
            }

            socket_close($socket);
            if ($sts) {
                echo "<h1>Dados enviados com sucesso! Freezer atualizado!</h1>";
            } else {
                echo "<h1>Falha ao enviar os dados</h1>";
            }
        ?>
    <body>
</html>