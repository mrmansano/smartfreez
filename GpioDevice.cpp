
/*****************************************************************************/
/**
 * \file	GpioDevice.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/
/* Application includes */
#include <sstream>
#include "GpioDevice.h"

/******************************************************************/
/*                       Local Database                           */
/******************************************************************/
/*obs: put here global vars (if you need them)*/

/******************************************************************/
/*                       Methods                                  */
/******************************************************************/
/*! Função para log e testes */
static void doLogPin(std::string text, GpioPinNum pin) {
	std::stringstream ss;
	ss << text << " pin " << pin;
	doLog(ss.str());
}

GpioDevice::GpioDevice() {
	mStarted = true;
}

GpioDevice::~GpioDevice() {
}

GpioPinValue GpioDevice::readPin(GpioPinNum pin) {
	if (checkPin(pin) && isStarted()) {
		return (bcm2835_gpio_lev(pin) == 0)? GPIO_PIN_LOW : GPIO_PIN_HIGH;
	} else {
		throw "GPIO pin read error";
	}
}

void GpioDevice::writePin(GpioPinNum pin, GpioPinValue data) {
	if (checkPin(pin) && isStarted()) {
		bcm2835_gpio_write(pin, data);
	} else {
		throw "GPIO pin write error";
	}
}

void GpioDevice::addPin(GpioPinNum pin, std::string name,
		GpioPinDirection direction) {
	if (checkPin(pin)) {
		throw "GPIO pin already set";
	}

	if (isStarted()) {
		GpioPinInfo info;
		info.direction = direction;
		info.name = name;
		// Seleciona a direção do pino
		bcm2835_gpio_fsel(pin, direction);

		mPinMap[pin] = info;
	} else {
		throw "Can't add GPIO pin";
	}
}

void GpioDevice::remPin(GpioPinNum pin) {
	if (!checkPin(pin)) {
		throw "GPIO pin not set";
	}

	if (isStarted()) {
		mPinMap.erase(pin);
	} else {
		throw "Can't remove GPIO pin";
	}
}

bool GpioDevice::checkPin(GpioPinNum pin) {
	//  Busca o pino no mapa para ver se ele já foi registrado
	return (mPinMap.find(pin) != mPinMap.end());
}

bool GpioDevice::isStarted(){
	return mStarted;
}

void GpioDevice::setStarted(bool started) {
	mStarted = started;
}

void GpioDevice::changePinDirection(GpioPinNum pin, GpioPinDirection direction) {
	if (checkPin(pin) && isStarted()) {
		mPinMap[pin].direction = direction;
		bcm2835_gpio_fsel(pin, direction); // Seleciona a direção do pino
	}
}

int GpioDevice::waitRampDown(GpioPinNum pin, int timeout) {
	if (checkPin(pin) && isStarted()) {
		uint8_t timeCount = 0;
		GpioPinValue lastState = readPin(pin);
		// Nível já esta em baixa, não é necessário continuar
		if (lastState != GPIO_PIN_HIGH) return -timeout;
		// Apenas saira do loop quando acontecer a mudança de nível
		while ((lastState == GPIO_PIN_HIGH) && (timeCount < timeout)) {
				timeCount++;
				bcm2835_delayMicroseconds(1); // Espera 1 uS
				lastState = readPin(pin); // Le o pino para verificar se houve mudança
		}
		return timeCount;
	}
	return -timeout;
}

int GpioDevice::waitRampUp(GpioPinNum pin, int timeout) {
	if (checkPin(pin) && isStarted()) {
		uint8_t timeCount = 0;
		GpioPinValue lastState = readPin(pin);
		// Nível já esta em alta, não é necessário continuar
		if (lastState != GPIO_PIN_LOW) return -timeout;
		// Apenas saira do loop quando acontecer a mudança de nível
		while ((lastState == GPIO_PIN_LOW) && (timeCount < timeout)) {
				timeCount++;
				bcm2835_delayMicroseconds(1); // Espera 1 uS
				lastState = readPin(pin); // Le o pino para verificar se houve mudança
		}
		return timeCount;
	}
	return -timeout;
}
