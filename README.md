Mansano's Creations Smart Freez
===============================

![Mansano's Creations](./Fridge/MansanoCreationsSymbol.png)

** Warning: this project is a detached HEAD from the main repository **

Mansano's Creations Smart Freez is a small freezer controled and connected to the
internet. Using a raspberry-pi, a DHT-11 sensor, a peltier plate and a cooler it can refrigerate
foods and driks and warn the user about it's state.

This project uses Yocto to generate a firmware to the raspberry-pi with the main
project dependencies:

  * PHP5
  * Apache
  * C++ STL lib
  * bcm2835 libs
