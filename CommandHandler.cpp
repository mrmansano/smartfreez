
/*****************************************************************************/
/**
 * \file	CommandHandler.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

/* OS includes */
#include <iostream>
#include <algorithm>
#include <sstream>
#include <strings.h>
#include <string.h>
#include <unistd.h>

#include "CommandHandler.h"
#include "CommandCentral.h"

/******************************************************************/
/*                       Local Database                           */
/******************************************************************/
/*obs: put here global vars (if you need them)*/

/******************************************************************/
/*                       Methods                                  */
/******************************************************************/

CommandHandler::CommandHandler() {
	setSock();
}

CommandHandler::~CommandHandler() {
	close(mSock);
}

void CommandHandler::waitCommand() {
	while (true) {
		SocketDesc clientSock;
		struct sockaddr_in client;
		int socketSize = sizeof(struct sockaddr_in);
		int readSize;
		char message[MAX_MESSAGE_SIZE];

		listen(mSock, 1);
		if ((clientSock = accept(mSock, (struct sockaddr*)&client, (socklen_t*)&socketSize)) < 0) {
			throw "Can't accept client";
		}
		std::cout << "Client accepted..." << std::endl;
		while ((readSize = recv(clientSock, message, MAX_MESSAGE_SIZE, 0)) > 0) {
			std::string dataRecv = message;
			dataRecv.erase(std::remove_if(dataRecv.begin(), dataRecv.end(), ::isspace), dataRecv.end());

			std::cout << "Recv = " << dataRecv << std::endl;
			if (dataRecv.find(MESSAGE_GET_DATA) != std::string::npos) {
				std::string bData = CommandCentral::getInstance()->getSensorsData();
				write(clientSock, bData.c_str(), SEND_DATA_SIZE);
			} else if (handleMessage(dataRecv) == MSG_HANDLED) {
				write(clientSock, MESSAGE_SEND_ACK, SEND_MSG_SIZE);
			} else {
				write(clientSock, MESSAGE_SEND_ERROR, SEND_MSG_SIZE);
			}
			memset(message, '\0', MAX_MESSAGE_SIZE * sizeof(char));
		}
		if (readSize == 0) {
			close(clientSock);
		}
	}

}

SocketDesc CommandHandler::getSock() const {
	return mSock;
}

MessageHandleSts CommandHandler::handleMessage(std::string msg) {
	CommandCentral* cmdCentral = CommandCentral::getInstance();
	if (msg.find(MESSAGE_ON) != std::string::npos) {
		std::cout << "Turn On" << std::endl;
		cmdCentral->setRunning(true);
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_OFF) != std::string::npos) {
		std::cout << "Turn On" << std::endl;
		cmdCentral->setRunning(false);
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_PELT_FREEZE) != std::string::npos) {
		std::cout << "Peltier Freeze" << std::endl;
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_PELT_WARM) != std::string::npos) {
		std::cout << "Peltier Warm" << std::endl;
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_FAN_ON) != std::string::npos) {
		std::cout << "Peltier Freeze" << std::endl;
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_FAN_OFF) != std::string::npos) {
		std::cout << "Peltier Freeze" << std::endl;
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_PELT_FREEZE_VAL) != std::string::npos) {
		std::string value = msg.substr(msg.find("#") + 1, 2);
		std::cout << "Peltier Freeze value = " << value << std::endl;
		cmdCentral->setColdSideTempThresoldHigh(atoi(value.c_str()));
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_PELT_HOT_VAL) != std::string::npos) {
		std::string value = msg.substr(msg.find("#") + 1, 2);
		std::cout << "Peltier max Hot value = " << value << std::endl;
		cmdCentral->setHotSideTempThresoldLow(atoi(value.c_str()));
		return MSG_HANDLED;
	}
	if (msg.find(MESSAGE_CRITICAL_VAL) != std::string::npos) {
		std::string value = msg.substr(msg.find("#") + 1, 2);
		std::cout << "Peltier critical Hot value = " << value << std::endl;
		cmdCentral->setHotSideTempThresoldHigh(atoi(value.c_str()));
		return MSG_HANDLED;
	}

	return MSG_ERROR;
}

void CommandHandler::setSock() {
	mSock = socket(AF_INET, SOCK_STREAM, 0);
	if (mSock == -1) {
		throw "Can't open socket";
	}
	int err = 0;
	struct sockaddr_in server;
	bzero((char *) &server, sizeof(server));

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(SERVER_PORT);

	std::cout << "Socket openned " << mSock <<  ". Binding" << std::endl;

	if ((err = bind(mSock, (struct sockaddr*)&server, sizeof(server))) < 0) {
		std::stringstream ss;
		ss << "Can't bind socket. Error: " << err;
		throw ss.str().c_str();
	}
}
