
/*****************************************************************************/
/**
 * \file	SensorsDefines.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

#ifndef SENSORSDEFINES_H_
#define SENSORSDEFINES_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

/* OS includes */
#include <bcm2835.h>
#include <iostream>

#define GPIO_TIMEOUT 1000
#define SENSORS_I2C_CLOCK_DIVIER BCM2835_I2C_CLOCK_DIVIDER_148

inline void doLog(std::string text) {
	std::cout << __FILE__ << " [" << __LINE__ << "] : " << text << std::endl;
}

typedef enum {
	GPIO_PIN_03 = RPI_V2_GPIO_P1_03,
	GPIO_PIN_05 = RPI_V2_GPIO_P1_05,
	GPIO_PIN_07 = RPI_V2_GPIO_P1_07,
	GPIO_PIN_08 = RPI_V2_GPIO_P1_08,
	GPIO_PIN_10 = RPI_V2_GPIO_P1_10,
	GPIO_PIN_11 = RPI_V2_GPIO_P1_11,
	GPIO_PIN_12 = RPI_V2_GPIO_P1_12,
	GPIO_PIN_13 = RPI_V2_GPIO_P1_13,
	GPIO_PIN_15 = RPI_V2_GPIO_P1_15,
	GPIO_PIN_16 = RPI_V2_GPIO_P1_16,
	GPIO_PIN_18 = RPI_V2_GPIO_P1_18,
	GPIO_PIN_19 = RPI_V2_GPIO_P1_19,
	GPIO_PIN_21 = RPI_V2_GPIO_P1_21,
	GPIO_PIN_22 = RPI_V2_GPIO_P1_22,
	GPIO_PIN_23 = RPI_V2_GPIO_P1_23,
	GPIO_PIN_24 = RPI_V2_GPIO_P1_24,
	GPIO_PIN_26 = RPI_V2_GPIO_P1_26,
} GpioPinNum;

typedef enum {
	GPIO_PIN_INPUT = BCM2835_GPIO_FSEL_INPT,
	GPIO_PIN_OUTPUT = BCM2835_GPIO_FSEL_OUTP,
	GPIO_PIN_ALT5 = BCM2835_GPIO_FSEL_ALT5
} GpioPinDirection;

typedef enum {
	GPIO_PIN_LOW = LOW,
	GPIO_PIN_HIGH = HIGH
} GpioPinValue;

#endif /* SENSORSDEFINES_H_ */
