/*****************************************************************************/
/**
 * \file	I2CDevice.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

#ifndef I2CDEVICE_H_
#define I2CDEVICE_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include <vector>
#include "SensorsDefines.h"

/******************************************************************/
/*                    Class Definition		                      */
/******************************************************************/
/*! Classe base para dispositivos I2C */
class I2CDevice {
public:
	/**
	 * \brief Construtor da classe
	 * \param address: Endereço I2C do dispositivo
	 * \param dataLen: Tamanho das palavras a serem trabalhadas
	 */
	I2CDevice(int address, int dataLen);
	/**
	 * \brief Destrutor da classe
	 */
	virtual ~I2CDevice();

	/**
	 * \brief Le os dados do registrador regAddr
	 * \param regAddr: Endereço do registrador para leitura no dispositivo
	 * \return Vetor com os dados lidos
	 */
	std::vector<char> readData(int regAddr);
	/**
	 * \brief Escreve os dados data
	 * \param data: dados a serem enviados para o dispositivo
	 */
	void writeData(char* data);

	/**
	 * \brief Retorna o endereço I2C do dispositivo
	 * \return O endereço I2C do dispositivo
	 */
	int getAddress() const;
	/**
	 * \brief Seta o endereço I2C do dispositivo
	 * \param address: O endereço I2C do dispositivo
	 */
	void setAddress(int address);
	/**
	 * \brief Retorna true se o dispositivo foi inicializado
	 * \return true ou false, conforme o estado do dispositivo
	 */
	bool isStarted() const;
	/**
	 * \brief Define como verdadeiro ou falso o valor de inicializado do dispositivo
	 * \param: stated: true ou false, conforme o estado do dispositivo
	 */
	void setStarted(bool started);
	/**
	 * \brief Pega o tamanho de dados a serem trabalhado com o dispositivo em 1 burst
	 * \return Valor inteiro do tamanho dos dados (bytes)
	 */
	int getDataLen() const;
	/**
	 * \brief Seta o tamanho de dados a serem trabalhado com o dispositivo em 1 burst
	 * \param dataLen: Valor inteiro do tamanho dos dados
	 */
	void setDataLen(int dataLen);

private:
	int mAddress; /*! Endereço I2C do dispositivo */
	int mDataLen; /*! Tamanho dos dados, em bytes */
	bool mStarted; /*! Estado de inicialização do dispositivo */
};

#endif /* I2CDEVICE_H_ */
