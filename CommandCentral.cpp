/*
 * CommandCentral.cpp
 *
 *  Created on: 04/05/2014
 *      Author: marcelo
 */

#include <unistd.h>
#include <iostream>
#include <sstream>
#include "CommandCentral.h"

CommandCentral* CommandCentral::mInstance = 0;

CommandCentral::CommandCentral() {
	mMonitoring = false;
	mRunning = false;
	// Valores default para os limiares
	mHotSideTempThresoldHigh = 40;
	mHotSideTempThresoldLow = 20;
	mColdSideTempThresoldLow = 0;
	mColdSideTempThresoldHigh = 10;
	mRelativeHumidityThreshold = 99;
}

void CommandCentral::startModules() {
	if (!bcm2835_init()) {
		throw "Can't start GPIO/i2c module";
	}
	// Set i2c clock divider value
	bcm2835_i2c_setClockDivider(SENSORS_I2C_CLOCK_DIVIER);

	mDht11Cold = new Dht11Device(COMMAND_CENTRAL_DHT11_COLD_PIN);
	mDht11Hot = new Dht11Device(COMMAND_CENTRAL_DHT11_HOT_PIN);
	mFanCtrl = new FanControl(COMMAND_CENTRAL_FAN_PIN);
	mPeltierCtrl = new PeltierControl();

	LCDDataPins dataPins = {GPIO_PIN_24, GPIO_PIN_22, GPIO_PIN_18, GPIO_PIN_16};
	mLcd = new LCDModule(GPIO_PIN_08, GPIO_PIN_26, dataPins);

	mLcd->initLCD();
	mLcd->writeText("Iniciando...", 0, 0);

	mAlarmLed = new GpioDevice;
	mAlarmLed->addPin(COMMAND_CENTRAL_ALARM_LED_PIN, "ALARM", GPIO_PIN_OUTPUT);
}

void CommandCentral::stopModules() {
	bcm2835_close();
}

CommandCentral::~CommandCentral() {
	// TODO Auto-generated destructor stub
}

void CommandCentral::monitor() {
	mMonitoring = true;
	mRunning = true;
	sem_init(&mutex, 0, 1);
	try {
		startModules();

		while (true) {
			if (isRunning()) {
				setAlarmOff();
				int tempColdSide = 0;
				int tempHotSide = 0;
				int humidity = 0;

				sleep(COMMAND_CENTRAL_MONITOR_INT);

				sem_wait(&mutex);
				// 1) We read the sensors to check the status
				try {
					mDht11Cold->readData();
					usleep(100000);
					mDht11Hot->readData();
					// 1.1 ) Checa temperaturas
					tempColdSide = mDht11Cold->getTemperature();
					tempHotSide = mDht11Hot->getTemperature();
					// 1.2 ) Checa umidade
					humidity = mDht11Cold->getHumidity();

					std::cout << "Temp Cold Side = " << tempColdSide << std::endl;
					std::cout << "Temp Hot Side = " << tempHotSide << std::endl;
					std::cout << "Humidity = " << humidity << std::endl;

					std::stringstream ss;
					mLcd->clear();
					ss << "Temp C=" << tempColdSide << " H=" << tempHotSide;
					mLcd->writeText(ss.str().c_str(), 0, 0);
					ss.str("");
					ss << "Humidity= " << humidity << "%";
					mLcd->writeText(ss.str().c_str(), 1, 0);

					// 2) Controla a FAN e a peltier, conforme a máquina de estados em anexo
					if (humidity > mRelativeHumidityThreshold) {
						std::cout << "Locked!" << std::endl;
						stopAll();
					} else {
						if (tempHotSide >= mHotSideTempThresoldLow) {
							std::cout << "Turn on FAN"<< std::endl;
							mFanCtrl->turnOn();
						}

						if (tempHotSide < mHotSideTempThresoldLow) {
							std::cout << "Turn off FAN"<< std::endl;
							mFanCtrl->turnOff();
						}

						if (tempHotSide >= mHotSideTempThresoldHigh) {
							std::cout << "Peltier Warm"<< std::endl;
							mPeltierCtrl->warm();
						}

						if (tempColdSide >= mColdSideTempThresoldHigh) {
							std::cout << "Peltier Freeze"<< std::endl;
							mPeltierCtrl->freeze();
						}

						if (tempColdSide < mColdSideTempThresoldLow) {
							std::cout << "Peltier Warm"<< std::endl;
							mPeltierCtrl->warm();
						}
					}
				} catch (const char* strEx) {
					std::cerr << "[Error]: " << strEx << std::endl;
				}
				sem_post(&mutex);
			}
		}
	} catch (const char* strEx) {
		std::cerr << "[Error]: " << strEx << std::endl;
	}
	stopAll();
}

void CommandCentral::stopAll() {
	// Para todos os processes do freezer
	mPeltierCtrl->turnOff();
	mFanCtrl->turnOff();
	mAlarmLed->writePin(COMMAND_CENTRAL_ALARM_LED_PIN, GPIO_PIN_HIGH);
	setRunning(false);
}

void CommandCentral::setAlarmOff() {
	mAlarmLed->writePin(COMMAND_CENTRAL_ALARM_LED_PIN, GPIO_PIN_LOW);
}

std::string CommandCentral::getSensorsData() {
	std::stringstream ss;
	sem_wait(&mutex);
	// Monta a string para a interface web
	ss << ((isRunning())?"1":"0") << ";";
	ss << mDht11Cold->getTemperature() << ";";
	ss << mDht11Hot->getTemperature() << ";";
	ss << mDht11Cold->getHumidity() << ";";
	ss << ((mFanCtrl->getStatus() == FAN_ON)?"1":"0") << ";";
	ss << mPeltierCtrl->getCurrentValue();
	sem_post(&mutex);
	return ss.str();
}
