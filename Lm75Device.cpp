
/*****************************************************************************/
/**
 * \file	Lm75Device.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include "Lm75Device.h"

/******************************************************************/
/*                       Local Database                           */
/******************************************************************/
/*obs: put here global vars (if you need them)*/

/******************************************************************/
/*                       Methods                                  */
/******************************************************************/

Lm75Device::Lm75Device(int i2cAddress, int i2cDataLen, std::string name):
	I2CDevice( i2cAddress, i2cDataLen ) {

	mName = name;
}

Lm75Device::~Lm75Device() {
}

int Lm75Device::calculateTemperature() {

	// Data format: 16bits
	// MSB temperature signal : 0 positive, 1 negative
	// bit 15 to 5: temperature steps

	// convertion: signal * temperature steps * 0.125

	std::vector<char> sensorReading = readData(LM75_TEMP_REGISTER);

	int signal = (sensorReading[0] & LM75_MSB_SIGNAL_MASK) ? -1 : 1;

	int step = LM75_TEMP_MASK;

	step &= (sensorReading[0] << LM75_MSB_SHIFT_LEN);
	step |= (sensorReading[1] >> LM75_LSB_SHIFT_LEN);

	mTemperature = signal * step * LM75_RESOLUTION;

	return mTemperature;
}
