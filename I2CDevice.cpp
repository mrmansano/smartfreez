
/*****************************************************************************/
/**
 * \file	I2CDevice.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include <sstream>

#include "I2CDevice.h"

/******************************************************************/
/*                       Local Database                           */
/******************************************************************/
/*obs: put here global vars (if you need them)*/

/******************************************************************/
/*                       Methods                                  */
/******************************************************************/

I2CDevice::I2CDevice(int address, int dataLen) {
	mAddress = address;
	mDataLen = dataLen;
}

I2CDevice::~I2CDevice() {
}

int I2CDevice::getAddress() const {
	return mAddress;
}

void I2CDevice::setAddress(int address) {
	mAddress = address;
}

bool I2CDevice::isStarted() const {
	return mStarted;
}

void I2CDevice::setStarted(bool started) {
	mStarted = started;
}

int I2CDevice::getDataLen() const {
	return mDataLen;
}

std::vector<char> I2CDevice::readData(int regAddr) {
	int code = 0;
	char data[mDataLen];
	char regAddrData = (char) regAddr;
	// Seta o endereço do slave como sendo o endereço I2C deste dispositivo
	bcm2835_i2c_setSlaveAddress(mAddress);
	// Começa a enviar os dados
	bcm2835_i2c_begin();
	// Realiza a leitura do regstrador
	code = bcm2835_i2c_read_register_rs(&regAddrData, data, mDataLen);

	std::stringstream ss;
	ss << "I2C read returned with status " << code;
	doLog(ss.str());
	// Finaliza a operação I2C
	bcm2835_i2c_end();

	return std::vector<char>(data, data + sizeof(data)/sizeof(data[0]));
}

void I2CDevice::writeData(char* data) {
	int code = 0;
	// Seta o endereço do slave como sendo o endereço I2C deste dispositivo
	bcm2835_i2c_setSlaveAddress(mAddress);
	// Começa a enviar os dados
	bcm2835_i2c_begin();
	// Realiza a escrita de data
	code = bcm2835_i2c_write(data, mDataLen);

	std::stringstream ss;
	ss << "I2C read returned with status " << code;
	doLog(ss.str());
	// Finaliza a operação I2C
	bcm2835_i2c_end();
}

void I2CDevice::setDataLen(int dataLen) {
	mDataLen = dataLen;
}
