/*****************************************************************************/
/**
 * \file	Dht11Device.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

#ifndef DHT11DEVICE_H_
#define DHT11DEVICE_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include "GpioDevice.h"

/******************************************************************/
/*                    Class Definition		                      */
/******************************************************************/
#define DHT11_TIME_TOLERANCE 10
#define DHT11_DATA_BITS 40
#define DHT11_END_OF_FRAME -1
/*! Struct que descreve os valores retornados pelo DHT11 */
typedef struct {
	union {
		uint8_t temperature_val; // Valor da temperatura
		uint8_t temperature_dec; // Casas decimais do valor de temperatura
		uint8_t humidity_val; // Valor da umidade
		uint8_t humidity_dec; // Valor decimal da umidade
	} details;
	uint32_t value; // Valor composto
} Dht11Data;

/*! Classe do sensor de temperatura e umidade DHT11 */
class Dht11Device: public GpioDevice {
public:
	/**
	 * \brief Construtor da Classe
	 * \param dataPin: Pino utilizado para comunicação com o sensor
	 */
	Dht11Device(GpioPinNum dataPin);
	/**
	 * \brief Destrutor da classe
	 */
	virtual ~Dht11Device();

	/**
	 * \brief Realiza uma leitura do sensor e atualiza os dados
	 * \return dados lidos
	 */
	Dht11Data readData();

	/**
	 * \brief Pega o último valor de umidade setado
	 * \return valor da humidade em %
	 */
	float getHumidity() const;
	/**
	 * \brief Seta o último valor de umidade
	 * \param humidity: valor da humidade em %
	 */
	void setHumidity(float humidity);
	/**
	 * \brief Pega o último valor de temperatura setado
	 * \return valor da humidade em Celsius
	 */
	float getTemperature() const;
	/**
	 * \brief Seta o último valor de temperatura
	 * \param temperature: valor da humidade em Celsius
	 */
	void setTemperature(float temperature);
	/**
	 * \brief Pega qual o pino utilizado para leitura dos dados do sensor
	 * \return pino escolhido para leitura do sensor
	 */
	GpioPinNum getDataPin() const;
	/**
	 * \brief Seta qual o pino utilizado para leitura dos dados do sensor
	 * \param dataPin: pino escolhido para leitura do sensor
	 */
	void setDataPin(GpioPinNum dataPin);

private:
	float mTemperature; /*! Último valor de temperatura lido */
	float mHumidity; /*! Último valor de umidade lido */
	GpioPinNum mDataPin; /*! Pino utilizado como dados para leitura do sensor */
	/*! Definições de alguns tempos para o protocolo de leitura do sensor */
	struct {
		int STARTUP_DELAY;
		int REQUEST_MILI;
		int END_OF_TRANSMISSION;
	} dht11Time;
};

#endif /* DHT11DEVICE_H_ */
