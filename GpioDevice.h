/*****************************************************************************/
/**
 * \file	GpioDevice.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
#ifndef GPIODEVICE_H_
#define GPIODEVICE_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include <map>
#include <string>

#include "SensorsDefines.h"

/******************************************************************/
/*                    Class Definition		                      */
/******************************************************************/
/*! Struct das informações relacionadas ao pino GPIO */
typedef struct {
	std::string name;
	GpioPinDirection direction;
} GpioPinInfo;

/*! Classe base para dispositivos GPIO */
class GpioDevice {
public:
	/**
	 * \brief Construtor da classe
	 */
	GpioDevice();
	/**
	 * \brief Destrutor da classe
	 */
	virtual ~GpioDevice();

	/**
	 * \brief Le o valor do pino
	 * \param pin: Pino a ser feita a leitura
	 * \return Valor do pino (0 ou 1)
	 */
	virtual GpioPinValue readPin(GpioPinNum pin);
	/**
	 * \brief Escreve o valor no pino
	 * \param pin: Pino a ser feita a escrita
	 * \param data: Valor do pino (0 ou 1)
	 */
	virtual void writePin(GpioPinNum pin, GpioPinValue data);

	/**
	 * \brief Atribui um pino ao dispositivo GPIO
	 * \param pin: Pino
	 * \param name: Identificador para o pino
	 * \param direction: Direção (entrada ou saída) para o pino
	 */
	void addPin(GpioPinNum pin, std::string name, GpioPinDirection direction);
	/**
	 * \brief Remove o pino do dispositivo GPIO
	 * \param pin: Pino
	 */
	void remPin(GpioPinNum pin);

	/**
	 * \brief Muda a direção do pino (entrada ou saída)
	 * \param pin: Pino
	 * \param direction: Direção
	 */
	void changePinDirection(GpioPinNum pin, GpioPinDirection direction);

	/**
	 * \brief Checa se o pino esta associado ao dispositivo
	 * \param pin: Pino
	 * \return true se está associado, false se não
	 */
	bool checkPin(GpioPinNum pin);
	/**
	 * \brief Verifica se o dispositivo foi inicializado
	 * \return true se estiver inicializado, false se não
	 */
	bool isStarted();
	/**
	 * \brief Seta o estado de inicializado como truer ou false
	 * \param started: true se sim, false se não
	 */
	void setStarted(bool started);

	/**
	 * \brief Aguarda mudança de borda de alta para baixa no pino
	 * \param pin: Pino a ser observado
	 * \param timeout: Valor máximo de tempo para esperar acontecer a mudança de nível (em uS)
	 * \return tempo total para acontecer a mudança (em uS)
	 */
	int waitRampDown(GpioPinNum pin, int timeout = GPIO_TIMEOUT);
	/**
	 * \brief Aguarda mudança de borda de baixa para alta no pino
	 * \param pin: Pino a ser observado
	 * \param timeout: Valor máximo de tempo para esperar acontecer a mudança de nível (em uS)
	 * \return tempo total para acontecer a mudança (em uS)
	 */
	int waitRampUp(GpioPinNum pin, int timeout = GPIO_TIMEOUT);

private:
	std::map<GpioPinNum, GpioPinInfo> mPinMap; /*! Mapa de pinos do dispositivo */
	bool mStarted; /*! Estado atual do dispositivo */
};

#endif /* GPIODEVICE_H_ */
