/*****************************************************************************/
/**
 * \file	CommandHandler.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/

#ifndef COMMANDHANDLER_H_
#define COMMANDHANDLER_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

/* OS includes */
#include<sys/socket.h>
#include<arpa/inet.h>
#include <string>

/* DMlib includes */

/* Application includes */

/******************************************************************/
/*                    Class Definition		                      */
/******************************************************************/

#define SERVER_PORT 55555
/*! Definições das mensagens a serem trocadas com a interface web */
#define MAX_MESSAGE_SIZE 100
#define MESSAGE_ON "T_ON"
#define MESSAGE_OFF "T_OFF"
#define MESSAGE_PELT_FREEZE "T_FREEZE_"
#define MESSAGE_PELT_WARM "T_WARM_"
#define MESSAGE_FAN_ON "T_FAN_ON"
#define MESSAGE_FAN_OFF "T_FAN_OFF"
#define MESSAGE_PELT_FREEZE_VAL "V_FREEZE#"
#define MESSAGE_PELT_HOT_VAL "V_HOT#"
#define MESSAGE_CRITICAL_VAL "V_CRIT#"

#define MESSAGE_GET_DATA "G_DATA"

#define SEND_MSG_SIZE 5
#define SEND_DATA_SIZE 18
#define MESSAGE_SEND_ACK "S_ACK"
#define MESSAGE_SEND_ERROR "S_ERR"

/*! Enum com os estados de troca da mensagens */
typedef enum {
	MSG_HANDLED,
	MSG_ERROR
} MessageHandleSts;
/*! Definição de um tipo para descrição do socket */
typedef int SocketDesc;

/*! Classe de controle dos comandos recebidos via interface web */
class CommandHandler {
public:
	/**
	 * \brief Construtor da classe, que também inicializa o socket
	 */
	CommandHandler();
	/**
	 * \brief Destrutor da classe, que também inicializa o socket
	 */
	virtual ~CommandHandler();

	/**
	 * \brief Loop principal onde se é feito o aguardo para o recebimento dos comandos
	 */
	void waitCommand();
	/**
	 * \brief Cuida da ação a ser executada por cada comando
	 * \param msg: mensagem recebida via interface web
	 */
	MessageHandleSts handleMessage(std::string msg);
	/**
	 * \brief Pega o descritor do socket
	 * \param valor do descritor do socket
	 */
	SocketDesc getSock() const;
	/**
	 * \brief Inicializa o socket
	 */
	void setSock();

private:
	SocketDesc mSock; /*! Descritor do socket */
};

#endif /* COMMANDHANDLER_H_ */
