/*****************************************************************************/
/**
 * \file	Lm75Device.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
#ifndef LM75DEVICE_H_
#define LM75DEVICE_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include "I2CDevice.h"
#include <vector>


/******************************************************************/
/*                    Class Definition		                      */
/******************************************************************/
#define LM75_RESOLUTION 0.125
#define LM75_MSB_SHIFT_LEN 8
#define LM75_LSB_SHIFT_LEN 5
#define LM75_MSB_SIGNAL_MASK 0x80
#define LM75_TEMP_REGISTER 0x00
#define LM75_TEMP_MASK 0x0fff

/*! Classe para leitura dos sensores de temnperatura LM75 */
class Lm75Device : I2CDevice{
public:
	/**
	 * \brief Construtor da classe
	 * \param i2cAddress: Endereço I2C do CI
	 * \param i2cDataLen: Tamanho da palavra para leitura do dispositivo
	 * \param name: Nome do dispositivo, para log
	 */
	Lm75Device( int i2cAddress, int i2cDataLen, std::string name );
	/**
	 * \brief Destrutor da classe
	 */
	virtual ~Lm75Device();

	/**
	 * \brief Pega o valor de temperatura do CI e calcula o valor em escala LM75_RESOLUTION
	 * \return Valor da temperatura
	 */
	int calculateTemperature();

	/**
	 * \brief Pega o valor de temperatura armazenado da última leitura
	 * \return Valor de temperatura armazenado
	 */
	int getTemperature() {
		return calculateTemperature();
	}

	/**
	 * \brief Pega o nome do dispositivo
	 * \return Nome dado ao dispositivo
	 */
	const std::string& getName() {
		return mName;
	}

private:
	int mTemperature; /*! Temperatura atualizada na última leitura */
	std::string mName; /*! Nome do dispositivo */
};

#endif /* LM75DEVICE_H_ */
