/*****************************************************************************/
/**
 * \file	FanControl.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
#ifndef FANCONTROL_H_
#define FANCONTROL_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include "GpioDevice.h"

/******************************************************************/
/*                    Class Definition		                      */
/******************************************************************/
/*! Enum contendo os valores dos estados do FAN */
typedef enum {
	FAN_ON,
	FAN_OFF
} FanStatus;

/*! Classe de controle via GPIO do FAN */
class FanControl: public GpioDevice {
public:
	/**
	 * \brief Construtor da classe
	 * \param fanCtrlpin: Pino GPIO utilizado
	 */
	FanControl(GpioPinNum fanCrtlpin);
	/**
	 * \brief Destrutor da classe
	 */
	virtual ~FanControl();

	/**
	 * \brief Liga o FAN
	 */
	void turnOn();
	/**
	 * \brief Desliga o FAN
	 */
	void turnOff();
	/**
	 * \brief Retorna o estado atual do FAN
	 * \return estado do FAN
	 */
	FanStatus getStatus() const;
	/**
	 * \brief Seta o estado do FAN
	 * \param status: estado do FAN
	 */
	void setStatus(FanStatus status);
	/**
	 * \brief Retorna qual o pino de controle do FAN
	 * \return qual o pino de controle
	 */
	GpioPinNum getFanCrtlPin() const;
	/**
	 * \brief Seta qual o pino de controle do FAN
	 * \return fanCtrlPin: qual o pino de controle
	 */
	void setFanCrtlPin(GpioPinNum fanCrtlPin);

private:
	GpioPinNum mFanCrtlPin; /*! Pino de controle do FAN */
	FanStatus mStatus; /*! Estado atual do FAN */
};

#endif /* FANCONTROL_H_ */
