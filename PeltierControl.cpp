
/*****************************************************************************/
/**
 * \file	PeltierControl.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include "PeltierControl.h"
#include <unistd.h>

/******************************************************************/
/*                       Methods                                  */
/******************************************************************/

PeltierControl::PeltierControl() : GpioDevice(){
	mPeltierCtrlPin = GPIO_PIN_12;
	addPin(mPeltierCtrlPin, "PELTIER_CTRL", GPIO_PIN_ALT5); // Seleciona como ALT5 a função do pino (PWM)

	mCurrentValue = 0;

	/* Inicializa pino de PWM */
	bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_16); // Seleciona o clock de pwm como sendo 16 avo do clock do processador
	bcm2835_pwm_set_mode(PELT_PWM_CHANNEL, 1, 1); // Seleciona o canal 0 de PWM e ativa o modo
	bcm2835_pwm_set_range(PELT_PWM_CHANNEL, PELT_PWM_RANGE); // Seta a escala, sendo o valor máximo de 9 e o passo de 1
}

PeltierControl::~PeltierControl() {
}

GpioPinNum PeltierControl::getPeltierCtrlPin() const {
	return mPeltierCtrlPin;
}

PeltierStatus PeltierControl::getStatus() const {
	return mStatus;
}

void PeltierControl::turnOff() {
	mStatus = PELTIER_OFF;
	bcm2835_pwm_set_data(PELT_PWM_CHANNEL, 0);
}

void PeltierControl::freeze() {
	mStatus = PELTIER_FREEZING;
	/* Incrementa em 1 (10%) o valor de resfriamento, sendo o máximo == 9 */
	bcm2835_pwm_set_data(PELT_PWM_CHANNEL, (mCurrentValue + 1 > (PELT_PWM_RANGE - 1))?(PELT_PWM_RANGE - 1):(++mCurrentValue));
	usleep(50);
}

void PeltierControl::warm() {
	mStatus = PELTIER_WARMING;
	/* Decrementa em 1 (10%) o valor de resfriamento, sendo o mínimo == 0 */
	bcm2835_pwm_set_data(PELT_PWM_CHANNEL, (mCurrentValue - 1 < 0)?0:(--mCurrentValue));
	usleep(50);
}

void PeltierControl::setStatus(PeltierStatus status) {
	mStatus = status;
}
