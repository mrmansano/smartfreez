/*
 * CommandCentral.h
 *
 *  Created on: 04/05/2014
 *      Author: marcelo
 */

#ifndef COMMANDCENTRAL_H_
#define COMMANDCENTRAL_H_

#include <semaphore.h>

#include "GpioDevice.h"
#include "Dht11Device.h"
#include "PeltierControl.h"
#include "FanControl.h"
#include "SensorsDefines.h"
#include "LCDModule.h"

/*! Definições dos pinos utilizados no trabalho */
#define COMMAND_CENTRAL_DHT11_COLD_PIN GPIO_PIN_03
#define COMMAND_CENTRAL_DHT11_HOT_PIN GPIO_PIN_05
#define COMMAND_CENTRAL_FAN_PIN GPIO_PIN_10
#define COMMAND_CENTRAL_PELTIER_PIN GPIO_PIN_12
#define COMMAND_CENTRAL_ALARM_LED_PIN GPIO_PIN_15
/*! Valor do intervalor de monitoramento */
#define COMMAND_CENTRAL_MONITOR_INT 4

/*! Classe singleton que cuida da máquina de estados do freezer e controla os dispositivos e sensores */
class CommandCentral {
private:
	/**
	 * \brief Construtor da classe, privado por ser singleton
	 */
	CommandCentral();

public:
	/**
	 * \brief Inicializa os módulos e a biblioteca de acesso aos GPIO bcm2835
	 */
	void startModules();
	/**
	 * \brief Para todos os módulos
	 */
	void stopModules();
	/**
	 * \brief Retorna uma instancia da classe, se ela não existir, cria.
	 * 	 	  Esta é a base para as classes do tipo singleton.
	 */
	static CommandCentral* getInstance() {
		if (!mInstance) {
			mInstance = new CommandCentral;
		}
		return mInstance;
	}
	/**
	 * \brief Destrutor da classe
	 */
	virtual ~CommandCentral();

	/**
	 * \brief Rotina de monitoramento e controle, deve ser chamada para iniciar o funcionamento
	 */
	void monitor();
	/**
	 * \brief Para a leitura dos sensores e desliga os dispositivos associados
	 */
	void stopAll();

	bool isMonitoring() const {
		return mMonitoring;
	}

	bool isRunning() const {
		return mRunning;
	}

	void setRunning(bool running) {
		mRunning = running;
	}

	void setAlarmOff();

	int getColdSideTempThresoldHigh() const {
		return mColdSideTempThresoldHigh;
	}

	void setColdSideTempThresoldHigh(int coldSideTempThresoldHigh) {
		mColdSideTempThresoldHigh = coldSideTempThresoldHigh;
	}

	int getColdSideTempThresoldLow() const {
		return mColdSideTempThresoldLow;
	}

	void setColdSideTempThresoldLow(int coldSideTempThresoldLow) {
		mColdSideTempThresoldLow = coldSideTempThresoldLow;
	}

	int getHotSideTempThresoldHigh() const {
		return mHotSideTempThresoldHigh;
	}

	void setHotSideTempThresoldHigh(int hotSideTempThresoldHigh) {
		mHotSideTempThresoldHigh = hotSideTempThresoldHigh;
	}

	int getHotSideTempThresoldLow() const {
		return mHotSideTempThresoldLow;
	}

	void setHotSideTempThresoldLow(int hotSideTempThresoldLow) {
		mHotSideTempThresoldLow = hotSideTempThresoldLow;
	}

	int getRelativeHumidityThreshold() const {
		return mRelativeHumidityThreshold;
	}

	void setRelativeHumidityThreshold(int relativeHumidityThreshold) {
		mRelativeHumidityThreshold = relativeHumidityThreshold;
	}

	/**
	 * \brief Retorna uma string com os dado dos sensores e dos dispositivos
	 * \return string com os dados lidos, para serem utilizados na interface web
	 */
	std::string getSensorsData();
private:
	static CommandCentral* mInstance; /*! Instancia única desta classe */

	int mTolerance; /*! Valor de tolerância, não utilizado */
	int mColdSideTempThresoldLow; /*! Limiar mínimo em Celsius do valor frio da Peltier */
	int mColdSideTempThresoldHigh; /*! Limiar máximo em Celsius do valor frio da Peltier */
	int mHotSideTempThresoldLow; /*! Limiar mínimo (seguro) em Celsius do valor quente da Peltier */
	int mHotSideTempThresoldHigh; /*! Limiar máximo (desligamento) em Celsius do valor quente da Peltier */
	int mRelativeHumidityThreshold; /*! Limiar de umidade máximo para desligamento do sistema */

	bool mMonitoring; /*! Flag de monitoramento */
	bool mRunning; /*! Flag de funcionamento */
	Dht11Device* mDht11Cold; /*! Dispositivo utilizado para ler temperatura e umidade do lado frio da Peltier */
	Dht11Device* mDht11Hot; /*! Dispositivo utilizado para ler temperatura do lado quente da Peltier */
	PeltierControl* mPeltierCtrl; /*! Objeto para controle da Peltier */
	FanControl* mFanCtrl; /*! Objeto para controle do Fan */
	GpioDevice* mAlarmLed; /*! Objeto para acionamento do alarme de emergência */
	LCDModule* mLcd; /*! Objeto para controle do LCD */

	sem_t mutex; /*! Mutex para acesso aos dispositivos, apenas por segurança */
};

#endif /* COMMANDCENTRAL_H_ */
