
/*****************************************************************************/
/**
 * \file	Dht11Device.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
/******************************************************************/
/*                       Includes                                 */
/******************************************************************/
/* Application includes */
#include <ctime>
#include <unistd.h>
#include <sys/time.h>
#include <sstream>
#include <vector>
#include "Dht11Device.h"

/******************************************************************/
/*                       Local Database                           */
/******************************************************************/
/*obs: put here global vars (if you need them)*/

/******************************************************************/
/*                       Methods                                  */
/******************************************************************/

Dht11Device::Dht11Device(GpioPinNum dataPin) : GpioDevice(){
	dht11Time.REQUEST_MILI = 20 * 1000; // 20ms
	dht11Time.STARTUP_DELAY = 500 * 1000; // 500 ms
	dht11Time.END_OF_TRANSMISSION = 1000;

	mDataPin = dataPin;
	addPin(mDataPin, "DATA", GPIO_PIN_INPUT);
}

Dht11Device::~Dht11Device() {
}

Dht11Data Dht11Device::readData() {
	Dht11Data dataRet;
	const int MAXTIMINGS = 100;
	int data[MAXTIMINGS];
	int bitidx = 0;
	int counter = 0;
	int laststate = GPIO_PIN_HIGH;
	int j=0;

	// Seta GPIO para saída
	changePinDirection(mDataPin, GPIO_PIN_OUTPUT);
	writePin(mDataPin, GPIO_PIN_HIGH);
	usleep(dht11Time.STARTUP_DELAY); // Aguarda 500ms
	writePin(mDataPin, GPIO_PIN_LOW); // Seta em baixa
	usleep(dht11Time.REQUEST_MILI); // Aguarda 20 ms

	changePinDirection(mDataPin, GPIO_PIN_INPUT); // Muda pino para entrada e começa a receber os dados

	data[0] = data[1] = data[2] = data[3] = data[4] = 0; // Vetor para armazenamento dos dados recebidos

	// Aguarda o valor do pino baixar de 1 para 0
	while (readPin(mDataPin) == 1) {
		usleep(1); // 1 uS
	}

	// Recebe os dados e atribui ao vetor
	for (int i=0; i< MAXTIMINGS; i++) {
		counter = 0;
		while ( readPin(mDataPin) == laststate) {
			counter++;
			// Se o tempo de espera ultrapassou o máximo, podemos parar a leitura
			if (counter == dht11Time.END_OF_TRANSMISSION)
				break;
		}
		laststate = readPin(mDataPin);
		if (counter == dht11Time.END_OF_TRANSMISSION) break;

		if ((i>3) && (i%2 == 0)) {
			// Coloca os bits recebidos no vetor de armazenamento, conforme recebido no protocolo
			data[j/8] <<= 1;
			if (counter > 200)
				data[j/8] |= 1;
			j++;
		}
	}
	/* Terminou a leitura. Atribui os valores lidos à struct de retorna e seta os valores internos,
	 * mas primeiro veirfica se o checksum bateu com os dados lidos.
	 */
	if ((j >= 39) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) ) {
		dataRet.details.humidity_val = data[0];
		dataRet.details.humidity_dec = data[1];
		dataRet.details.temperature_val = data[2];
		dataRet.details.temperature_dec = data[3];
		setTemperature(data[2]);
		setHumidity(data[0]);
	} else {
		/*
		dataRet.details.humidity_val = getHumidity();
		dataRet.details.humidity_dec = 0;
		dataRet.details.temperature_val = getTemperature();
		dataRet.details.temperature_dec = 0;
		*/
		throw "Read again";
	}

	return dataRet;
}

void Dht11Device::setTemperature(float temperature) {
	mTemperature = temperature;
}

float Dht11Device::getHumidity() const {
	return mHumidity;
}

void Dht11Device::setHumidity(float humidity) {
	mHumidity = humidity;
}

float Dht11Device::getTemperature() const {
	return mTemperature;
}

GpioPinNum Dht11Device::getDataPin() const {
	return mDataPin;
}

void Dht11Device::setDataPin(GpioPinNum dataPin) {
	mDataPin = dataPin;
}
