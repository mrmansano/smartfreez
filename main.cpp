/*****************************************************************************/
/**
 * \file	main.cpp
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
#include <iostream>
#include <pthread.h>

#include "CommandCentral.h"
#include "CommandHandler.h"

/**
 * \brief Callback da thread para monitoramento de comandos do socket
 * \param ptr: ponteiro para qualquer parâmetro a se passar para a thread
 */
static void *connectionManager(void* ptr) {
	try {
		CommandHandler cmdHdlr;
		cmdHdlr.waitCommand();
	} catch (const char* err) {
		std::cerr << err << std::endl;
	}
}

int main(int argc, char **argv) {
	try {
		// Inicia a thread para recepçãoa de comandos via socket
		pthread_t cnnThread;
		pthread_create(&cnnThread, 0, connectionManager, 0);

		// Incializa todos os módulos e portas pré-estabelecidos
		CommandCentral::getInstance()->startModules();
		// Inicia a rotinamento de monitoramento
		CommandCentral::getInstance()->monitor();

		pthread_join(cnnThread, 0);
	} catch (const char* s) {
		std::cerr << s << std::endl;
	}
	return 1;
}

