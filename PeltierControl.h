/*****************************************************************************/
/**
 * \file	PeltierControl.h
 * \author	marcelo.mansano
 * \brief 
 *
 * Purpose:  
 *
 */
/****************************************************************************/
#ifndef PELTIERCONTROL_H_
#define PELTIERCONTROL_H_

/******************************************************************/
/*                       Includes                                 */
/******************************************************************/

#include "GpioDevice.h"

/******************************************************************/
/*                    Class Definition		                      */
/******************************************************************/

#define PELT_PWM_RANGE 10
#define PELT_PWM_CHANNEL 0

/*! Enum dos estados válidos para a Peltier */
typedef enum {
	PELTIER_OFF,
	PELTIER_WARMING,
	PELTIER_FREEZING
} PeltierStatus;

/*! Classe de controle da Célula Peltier */
class PeltierControl: public GpioDevice {
public:
	/**
	 * \brief Construtor da classe PeltierControl
	 */
	PeltierControl();
	/**
	 * \brief Destrutor da classe PeltierControl
	 */
	virtual ~PeltierControl();

	/**
	 * \brief Desliga a célula peltier
	 */
	void turnOff();
	/**
	 * \brief Resfria o lado "frio" da célula em +10%, máximo 100%
	 */
	void freeze();
	/**
	 * \brief Aquece o lado "frio" da célula em -10%, mínimo 0%
	 */
	void warm();

	/**
	 * \brief Retorna o pino para controle da Peltier
	 * \return Valor do pino em GpioPinNum
	 */
	GpioPinNum getPeltierCtrlPin() const;

	/**
	 * \brief Retorna o status atual da célula Peltier
	 * \return Status em PeltierStatus
	 */
	PeltierStatus getStatus() const;

	/**
	 * \brief Seta o status atual da célula Peltier
	 * \param status: Status em PeltierStatus
	 */
	void setStatus(PeltierStatus status);

	/**
	 * \brief Pega o valor de resfriamento da Peltier numa escala de 1-10
	 * \return Valor de resfriamento
	 */
	int getCurrentValue() const {
		return mCurrentValue;
	}

	/**
	 * \brief Seta o valor de resfriamento da Peltier numa escala de 1-10
	 * \return currentValue: Valor de resfriamento
	 */
	void setCurrentValue(int currentValue) {
		mCurrentValue = currentValue;
	}

private:
	GpioPinNum mPeltierCtrlPin; /*! Pino de controle da célula peltier */
	PeltierStatus mStatus; /*! Status atual da célula */
	int mCurrentValue; /*! Valor de resfriamento */
};

#endif /* PELTIERCONTROL_H_ */
